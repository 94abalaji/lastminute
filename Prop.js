/*----------Prop.js--------
 * Contains code for prop
 * objects such as asteroids
 * or flares. 
 * ----------------------------
 * Author: Ashwin Balaji
 * Created: 3/7/2015
 * CMPM 20 Winter 2015
 * ----------------------------
 */


/* --PreloadProps()--
 * The preload snippet for 
 * this file's objects. Note
 * that this function is global 
 * and therefore has a handle
 * to 'game'.
 * Param: none
 * Return: none
 */
function preloadProps(){
	//Asteroid Preload
	game.load.image('asteroid1', 'asteroid1.png');
	game.load.image('asteroid2', 'asteroid2.png');
	game.load.image('asteroid3', 'asteroid3.png');
	
	//Asteroid Polygon Physics Data
	//AVOID: SLOWS THE GAME DRAMATICALLY
	/*
	game.load.physics('as_phys1','asteroid1_phys.json');
	game.load.physics('as_phys2','asteroid2_phys.json');
	game.load.physics('as_phys3','asteroid3_phys.json');
	*/
	//Flare Preload
	game.load.atlasJSONArray('flare','flare.png', 'flares.json');
	
}

function Prop(g){
	//Handle to game
	this.game = g;
	
	//Possible types of prop
	Prop.prototype.propType = {asteroid: 0, flare: 1};
	
	//Generate random prop type
	this.type = Math.floor((Math.random()*2));
	
	//sprite for prop
	this.spr = null;
	
	Prop.prototype.generateProp = function(){
		var x = Math.floor(Math.random()*(this.game.world.bounds.width-(740*2))) + 740;
		var y = Math.floor(Math.random()*(this.game.world.bounds.height -(740*2))) + 740;
		var r; 
				
		if(this.type == this.propType.asteroid){
			//set asteroid sprite
			this.sprVariation = Math.floor(Math.random()*3);
			r = Math.floor(Math.random()*128) + 64;
			this.spr = this.game.add.sprite(x,y,'asteroid'+(this.sprVariation+1));
			this.spr.anchor.setTo(.5);
			
			//console.log(this.spr.x + ' ' + this.spr.y + ' ' + r);
		}
		else{
			//set flare sprite
			r = Math.floor(Math.random()*64) + 64;
			this.spr  = this.game.add.sprite(x,y,'flare');
			this.spr.animations.add('glow',null,20*Math.floor((Math.random())+1),true,true);
			this.spr.anchor.setTo(.5);
			this.spr.animations.play('glow');
		}
		
		this.spr.width = r;
		this.spr.height = r; 
		
	};
	
	Prop.prototype.addToPhysics = function(){
		if(this.type == this.propType.asteroid){
			this.game.physics.p2.enable(this.spr,false);
			this.spr.body.setCircle(this.spr.width/2);
			this.spr.body.angle = Math.floor((Math.random()*360)-180);
			//this.spr.body.clearShapes();
			//this.spr.body.loadPolygon('as_phys'+(this.sprVariation+1),'asteroid'+(this.sprVariation+1));
         this.spr.body.setCollisionGroup(asteroidCollisionGroup);
         this.spr.body.collides([playerCollisionGroup, planetCollisionGroup, asteroidCollisionGroup]);
      }
		
	};
}

function hitAsteroid(body1, body2) {
    //  body1 is the space ship (as it's the body that owns the callback)
    //  body2 is the body it impacted with, in this case a planet
    //  As body2 is a Phaser.Physics.P2.Body object, you access its own (the sprite) via the sprite property:
    gui.health.setBarVal(ship.health, gui.health.spr, ship.health-=4);
    did_collide++;
    //console.log(ship.health);
}

function PropManager(g){
	this.game = g;
	this.props = [];
	this.numProps = 0;
	
	PropManager.prototype.populateList = function(n){
		for(var i = 0; i<n; i++){
			var p = new Prop(g);
			p.generateProp();
			p.addToPhysics();
			this.props.push(p);
		}
		this.numProps = n;
	};
	
	PropManager.prototype.clearList = function(){
		for(var i = 0; i<this.numProps; i++){
			this.props[i].spr.destroy();
			this.props.unshift();
		}
		numPlanets = 0;
	};
}
