# README #

Has the source code for Project: Last Minute, a simple game made in a week which explores physics systems in web browsers. The stage is made up of mostly empty space but there are planets and asteroids sprinkled randomly. Hitting objects will reduce the player's health and the game ends when the player's health is depleted. One aspect that was focused on had to do with requiring the player to rotate out of a planet's orbit as opposed to applying thrust perpendicular to the planet's surface.


### Setup ###

To use, download the files and double-click the index.html file in a file explorer.
NOTE: For some reason Google Chrome does not like to work with the asset loaders. Use firefox or internet explorer/edge.