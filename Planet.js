/*--------Planet.js----------
 * Contains code for planets.
 * --------------------------
 * Author: Ashwin Balaji
 * Created: 2/21/2015
 * CMPM 20 Winter 2015
 * --------------------------
 */

//ORBIT VARIABLES
var rdistance;
var gravF;
var stableorbitcounter = 0;
var singleorbitcounter = 0;
var currentsector = null;
var orbitdirection = null;
var previoussector = null;
var orbitingbody = null;
var orbitangle = null;
var did_collide = null;
var did_super_collide = null;
var currentplanet;

//Planet source file strings
	var planetSrc = {
		0: 'planet1_spr.png',
		1: 'planet2_spr.png',
		2: 'planet3_spr.png',
		3: 'planet4_spr.png',
		4: 'planet5_spr.png',
		5: 'planet6_spr.png',
		6: 'planet7_spr.png',
		7: 'planet8_spr.png',
		8: 'planet9_spr.png',
		9: 'planet10_spr.png'
	};
	
	//Global Fn (should not exist per instance)
	function preloadPlanets (){
		for(var i = 0; i<10; i++){
			//this.game.load.image('planet' + (i), planetSrc[i]);
			this.game.load.atlasJSONArray('planet'+(i),planetSrc[i],'planet1.json');
			//console.log('Debug: Planet' + (i) + ' loaded.');
		}
	};

//Planet constructor
function Planet(g){
	this.game = g;
		
	//The following is meant for
	//	UI interaction
	this.scanned = false;
	this.isEarth = false;
	this.mapped = false;
	this.pType = 0;
	
	this.spr = null;
	
	//NOTE: Adapt existing code from main in the following fns
	
	//Generate coordinates, randomize radius and add to minimap
	Planet.prototype.generatePlanet = function(){
		//WIP
		var x = Math.floor(Math.random()*(this.game.world.bounds.width-(740*2))) + 740;
		var y = Math.floor(Math.random()*(this.game.world.bounds.height -(740*2))) + 740;
		var r = Math.floor(Math.random()*512) + 128;
		this.pType = Math.floor(Math.random()*10);
		//console.log(x + ' ' + y + ' ' + this.pType + ' ' + r);
		
		this.spr = game.add.sprite(x,y,'planet' + this.pType);
		this.spr.width = r;
		this.spr.height = r;
		
		this.spr.animations.add('rotate',null,15*Math.floor((Math.random()*4)+1),true,true);
		this.spr.animations.play('rotate');
		this.spr.anchor.setTo(.5);
			
	};
	
	//Create physics proxies based on sprite sizes.
	Planet.prototype.addToPhysics = function(){
		this.game.physics.p2.enable(this.spr,false);
		this.spr.body.immovable = true;
		this.spr.body.setCircle(this.spr.width/2);
		this.spr.body.static = true;
		this.spr.body.angle = (Math.random()*360);	
      this.spr.body.setCollisionGroup(planetCollisionGroup);
      this.spr.body.collides([playerCollisionGroup, asteroidCollisionGroup]);      
	};
	
	//Alert minimap on discovery
	Planet.prototype.alertMinimap = function(mm){
		if(!this.mapped && this.spr.inCamera){
			mm.spr.addChild(game.make.sprite(this.spr.x, this.spr.y, 'planet'+this.pType));
			this.mapped = true;
		}
	};	
}

function hitPlanet(body1, body2) {

    //  body1 is the space ship (as it's the body that owns the callback)
    //  body2 is the body it impacted with, in this case a planet
    //  As body2 is a Phaser.Physics.P2.Body object, you access its own (the sprite) via the sprite property:
    var angle_diff = Math.abs( ship.spr.angle - orbitangle ); 
    if (angle_diff > 35 && angle_diff < 145){
       gui.health.setBarVal(ship.health, gui.health.spr, ship.health-=4);
       did_collide++;
       //console.log(ship.health);
    };
    if (Math.abs(angle_diff - 90) < 20){
       gui.health.setBarVal(ship.health, gui.health.spr, ship.health-=4);
       did_super_collide++;
    };
}

function PlanetManager(g){
	this.game = g;
	this.planets = [];
	this.earthLoc = null;
	this.numPlanets = 0;
	
	PlanetManager.prototype.getClosestPlanetTo = function(ship){
		var tmp = Number.MAX_VALUE;
		var index = 0;
		for(var i = 0; i<this.numPlanets; i++){
			if(this.planets[i].spr.inCamera){
				var dist = getDistance(ship,this.planets[i].spr);
				if(dist <= tmp){tmp = dist; index = i;}
			} 
		}
		//console.log(index + ' ' + this.planets[index].spr.x + ' ' + this.planets[index].spr.y);
		return this.planets[index];
	};
	
	PlanetManager.prototype.populateList = function(n){
		//param is to be set to numPlanets
		for(var i = 0; i<n; i++){
			var p = new Planet(g);
			p.generatePlanet();
			p.addToPhysics();
			this.planets.push(p);
		}
		this.numPlanets = n;
	};
	
	PlanetManager.prototype.clearList = function(){
		for(var i = 0; i<this.numPlanets; i++){
			this.planets[i].spr.destroy();
			this.planets.unshift();
		}
		numPlanets = 0;
	};
	
	PlanetManager.prototype.resolveOverlap = function(){		
		var counter = 0;
		for(var i = 0; i<this.numPlanets; i++){
			var pl1 = this.planets[i];
			for(var j = 0; j<this.numPlanets; j++){
				if(i == j)continue;
				var pl2 = this.planets[j];
				if(getDistance(pl1.spr,pl2.spr) <= (((pl1.spr.width/2)*1.8) + ((pl2.spr.width/2)*1.8))){
					//this.planets[i].generatePlanet();
					var x = Math.floor(Math.random()*(this.game.world.bounds.width-(740*2))) + 740;
					var y = Math.floor(Math.random()*(this.game.world.bounds.height -(740*2))) + 740;
					var r = Math.floor(Math.random()*512) + 128;
					pl1.spr.x = x;
					pl1.spr.y = y;
					pl1.width = r;
					pl1.height = r;
					if(counter < 2*this.numPlanets)j=0;
					else counter = 0;
					counter++;
				}
			}
		}
		//console.log(counter + 'c' + this.numPlanets);
	};
	
	PlanetManager.prototype.setEarthLoc = function(){
		var tmp = Math.floor(Math.random()*this.numPlanets);
		this.planets[tmp].isEarth = true;
		this.earthLoc = this.planets[tmp];
				
	};
	
	PlanetManager.prototype.alertUI = function(mm){
		for(var i = 0; i<this.numPlanets; i++){
			this.planets[i].alertMinimap(mm);
		}
	};
	
}

//ORBIT UTILITY FUNCTIONS

function setToOrbit(player, bigobject){
	if(bigobject.isEarth === true){
		console.log("you win");
		earth_txt.visible = true;
	}
	else{
		errsound.play('',0,1,false,false);;
		ship.fuel = 100;
		fuel_txt.visible = true;
	}
}

function gravitateToObject(obj1, obj2, gravPow) {
    if (typeof speed === 'undefined') { gravPow = 10000000; }
    var angle = Math.atan2(obj2.y - obj1.y, obj2.x - obj1.x);
    rdistance = Phaser.Point.distance(obj1, obj2) / (obj2.width/128);
    gravF = (gravPow  / (rdistance * rdistance));
    obj1.body.force.x += Math.cos(angle) * gravF;    // accelerateToObject 
    obj1.body.force.y += Math.sin(angle) * gravF;
}

function getDistance(player, bigobject){
	var xdist = 0;
	var ydist = 0;
	xdist = Math.pow(player.x - bigobject.x,2);
	ydist = Math.pow(player.x - bigobject.y,2);
	var totaldist = xdist + ydist;
	totaldist = Math.sqrt(totaldist);
	return totaldist;
}

/*	returns true if the celestial object should activate its gravity on the spaceship
*   @param	playerposx, x position of the player
*	@param playerposy, y position of the player
*	@param bigobject, the celestial object
*/
function shouldGravityHappen(playerposx, playerposy, bigobject){
	//console.log((bigobject.width * 1.25));
	if(playerposx < bigobject.x - /*400*/(bigobject.width * 1.5) || playerposx > bigobject.x + /*400*/(bigobject.width * 1.5)){
		return false;
	}
	if(playerposy < bigobject.y - /*400*/(bigobject.width * 1.5) || playerposy > bigobject.y + /*400*/(bigobject.width * 1.5)){
		return false;
	}
	return true;
}

// Returns if a collision is true
//NOTE: FIX THE OFFSETS TO BE DYNAMIC BASED ON PLANET SIZE
function isCollision(player,bigobject){
	if(player.x > bigobject.x - 90 && player.x < bigobject.x + 90){
		if(player.y > bigobject.y - 90 && player.y < bigobject.y + 90){
			return true;
		}
	}
	return false;
}

//used for the stable orbit detection, must stay within this range from the planet
function isInRange(player,bigobject){
	if(player.x > bigobject.x - (bigobject.width * 1.5 ) && player.x < bigobject.x + (bigobject.width * 1.5)){
		if(player.y > bigobject.y - (bigobject.width * 1.5) && player.y < bigobject.y + (bigobject.width * 1.5)){
			return true;
		}
	}
	return false;
}

function isStableOrbit(player, bigobject){
	if(!isInRange(player,bigobject)){
		stableorbitcounter = 0;
		singleorbitcounter = 0;
		currentsector = null;
		orbitdirection = null;
		if(currentplanet != bigobject){
			earth_txt.visible = false;
			fuel_txt.visible = false;
			scan_txt.visible = false;
		}
		currentplanet = bigobject;
		//console.log("not in range");
		return false;
	}
	if(currentplanet != bigobject){
			earth_txt.visible = false;
			fuel_txt.visible = false;
			scan_txt.visible = false;
		}
	currentplanet = bigobject;
	// find which sector its in
	//if its in the same sector do nothing
	// check it goes to a clockwise or anti clockwise sector
	
	// top left sector
	if(player.x < bigobject.x && player.y < bigobject.y){
		if(currentsector === "top left"){
			return false;
		}
		if(singleorbitcounter > 0){
			if(currentsector === "bottom left"){
				singleorbitcounter++;
				currentsector = "top left";
				//console.log("bot left to top left");
				//console.log("orbitcounter: " + singleorbitcounter);
				if(orbitdirection === "counterclockwise"){
					singleorbitcounter = 0;
					stableorbitcounter = 0;
					//console.log("direction reset");
				}
				orbitdirection = "clockwise";
			}
			else if(currentsector === "top right"){
				singleorbitcounter++;
				currentsector = "top left";
				//console.log("top right to top left");
				//console.log("orbitcounter: " + singleorbitcounter);
				if(orbitdirection === "clockwise"){
					singleorbitcounter = 0;
					stableorbitcounter = 0;
					//console.log("direction reset");
				}
				orbitdirection = "counterclockwise";
			}
			else{
				singleorbitcounter = 0;
				stableorbitcounter = 0;
				currentsector = "top left";
			}
		}
		if(singleorbitcounter === 0){
			currentsector = "top left";
			singleorbitcounter++;
			//console.log("single orbit top left");
		}
		if(singleorbitcounter >= 4){
			stableorbitcounter++;
			singleorbitcounter = 0;
			//console.log("single orbit counter is at 4");
		}
		if(stableorbitcounter === 4){
				stableorbitcounter = 0;
				return true;
		}
		//console.log("top left");
	}
	// bottom left sector
	if(player.x < bigobject.x && player.y > bigobject.y){
		if(currentsector === "bottom left"){
			return false;
		}
		if(singleorbitcounter > 0){
			if(currentsector === "bottom right"){
				singleorbitcounter++;
				currentsector = "bottom left";
				//console.log("bot right to bot left");
				//console.log("orbitcounter: " + singleorbitcounter);
				if(orbitdirection === "counterclockwise"){
					singleorbitcounter = 0;
					stableorbitcounter = 0;
					//console.log("direction reset");
				}
				orbitdirection = "clockwise";
			}
			else if(currentsector === "top left"){
				singleorbitcounter++;
				currentsector = "bottom left";
				//console.log("top reft to bottom left");
				//console.log("orbitcounter: " + singleorbitcounter);
				if(orbitdirection === "clockwise"){
					singleorbitcounter = 0;
					stableorbitcounter = 0;
					//console.log("direction reset");
				}
				orbitdirection = "counterclockwise";
			}
			else{
				singleorbitcounter = 0;
				stableorbitcounter = 0;
				currentsector = "bottom left";
			}
		}
		if(singleorbitcounter === 0){
			currentsector = "bottom left";
			singleorbitcounter++;
			//console.log("single orbit bottom left");
		}
		if(singleorbitcounter >= 4){
			stableorbitcounter++;
			singleorbitcounter = 0;
			//console.log("single orbit counter is at 4");
		}
		if(stableorbitcounter === 4){
				stableorbitcounter = 0;
				return true;
		}
		//console.log("bottom left");
	}
	
	// top right sector
	if(player.x > bigobject.x && player.y < bigobject.y){
		if(currentsector === "top right"){
			return false;
		}
		if(singleorbitcounter > 0){
			if(currentsector === "top left"){
				singleorbitcounter++;
				currentsector = "top right";
				//console.log("top left to top right");
				//console.log("orbitcounter: " + singleorbitcounter);
				if(orbitdirection === "counterclockwise"){
					singleorbitcounter = 0;
					stableorbitcounter = 0;
					//console.log("direction reset");
				}
				orbitdirection = "clockwise";
			}
			else if(currentsector === "bottom right"){
				singleorbitcounter++;
				currentsector = "top right";
				//console.log("bottom right to top right");
				//console.log("orbitcounter: " + singleorbitcounter);
				if(orbitdirection === "clockwise"){
					singleorbitcounter = 0;
					stableorbitcounter = 0;
					//console.log("direction reset");
				}
				orbitdirection = "counterclockwise";
			}
			else{
				singleorbitcounter = 0;
				stableorbitcounter = 0;
				currentsector = "top right";
			}
		}
		if(singleorbitcounter === 0){
			currentsector = "top right";
			singleorbitcounter++;
			//console.log("single orbit top right");
		}
		if(singleorbitcounter >= 4){
			stableorbitcounter++;
			singleorbitcounter = 0;
			//console.log("single orbit counter is at 4");
		}
		if(stableorbitcounter === 4){
				stableorbitcounter = 0;
				return true;
		}
		//console.log("top right");
	}
	// bottom right sector
	if(player.x > bigobject.x && player.y > bigobject.y){
		if(currentsector === "bottom right"){
			return false;
		}
		if(singleorbitcounter > 0){
			if(currentsector === "top right"){
				singleorbitcounter++;
				currentsector = "bottom right";
				//console.log("top right to bottom right");
				//console.log("orbitcounter: " + singleorbitcounter);
				if(orbitdirection === "counterclockwise"){
					singleorbitcounter = 0;
					stableorbitcounter = 0;
					//console.log("direction reset");
				}
				orbitdirection = "clockwise";
			}
			else if(currentsector === "bottom left"){
				singleorbitcounter++;
				currentsector = "bottom right";
				//console.log("bottom left to bottom right");
				//console.log("orbitcounter: " + singleorbitcounter);
				if(orbitdirection === "clockwise"){
					singleorbitcounter = 0;
					stableorbitcounter = 0;
					//console.log("direction reset");
				}
				orbitdirection = "counterclockwise";
			}
			else{
				singleorbitcounter = 0;
				stableorbitcounter = 0;
				currentsector = "bottom right";
			}
		}
		if(singleorbitcounter === 0){
			currentsector = "bottom right";
			singleorbitcounter++;
			//console.log("single orbit bottom right");
		}
		if(singleorbitcounter >= 4){
			stableorbitcounter++;
			singleorbitcounter = 0;
			//console.log("single orbit counter is at 4");
		}
		if(stableorbitcounter === 4){
				stableorbitcounter = 0;
				return true;
		}
		//console.log("bottom right");
		
	}
	
	
}