
var game = new Phaser.Game(1600, 900, Phaser.CANVAS, 'phaser-example', { preload: preload, create: create, update:update, render: render });
var ship = new Ship(game);
var gui = new GameUI(game);
var cursors;

var rdistance;
var gravF;
var stableorbitcounter = 0;
var singleorbitcounter = 0;
var currentsector = null;
var orbitdirection = null;
var previoussector = null;
var orbitingbody = null;
var minimap;
var miniship;

function preload() {
        //Preload assets
        game.load.image('backdrop', 'space_stars_backdrop.png');
        game.load.image('minimap_backdrop', 'stars_mm.jpg');
        preloadPlanets();
		ship.preloadShip();
		gui.preloadGUI();
        game.load.image('earth', 'planet2_down.png');
        game.load.image('planet1', 'planet1_down.png');
        game.load.image('miniship', 'rocket_spr_down.png');
}

function create() {
	game.add.tileSprite(0,0, 4096, 4096, 'backdrop');
	//game.world.setBounds(0, 0, 3700, 3700);
	game.world.setBounds(0, 0, 4096, 4096);
    //Planet p1 = new Planet(game);
    
    ship.initShip();
    gui.initGUI();
    
    game.physics.startSystem(Phaser.Physics.P2JS);
    cursors = game.input.keyboard.createCursorKeys();
    player = ship.spr;
    game.physics.p2.enable(player,false);
    //player = game.add.sprite(32, game.world.height - 150, 'ship');
    earth = game.add.sprite(500, 500, 'earth');
    
    game.physics.p2.enable([player,earth], false);
    player.body.immovable = true;
    
    earth.body.setCircle(earth.width/2);
    earth.body.static = true;
    game.camera.follow(player);
	// another earth to test the orbits.
	earth2 = game.add.sprite(1100,1100,'planet1');
	
	
	game.physics.p2.enable([player,earth2],false);
	earth2.body.setCircle(earth2.width/2);
	earth2.body.static = true;
	
	//NOTE: change backdrop to minimap_backdrop
	//minimap = game.add.tileSprite(0,0,4096,4500,'minimap_backdrop');
	minimap = game.add.tileSprite(0,0,8192,8192,'minimap_backdrop');
	
	minimap.fixedToCamera = true;
	minimap.scale.setTo(.06);
	minimap.cameraOffset.setTo(game.camera.width - 250,630);
	miniship  = minimap.addChild(game.make.sprite(player.x, player.y, 'miniship'));
	
	earth.mapped = false;
	earth2.mapped = false;
	
    ship.addFlames();
    
    
};

var limit = true;

function update() {
    if (cursors.left.isDown) {player.body.rotateLeft(100);}   //ship movement
    else if (cursors.right.isDown){player.body.rotateRight(100);}
    else {player.body.setZeroRotation();}
    if (cursors.up.isDown){player.body.thrust(400);}
    else if (cursors.down.isDown){player.body.reverse(400);}
    
    if((cursors.up.isDown || cursors.down.isDown)){
        if(limit == true ){
            if(!cursors.down.isDown)ship.startFlames();
        }
        limit = false;
    }
    else {ship.stopFlames(); limit = true;}
    
	// disable or enable gravity for earth1 within 400 units
	if(shouldGravityHappen(player.x, player.y, earth)){
		gravitateToObject(player, earth);
	}
	// disable or enable gravity for earth1 within 400 units
	if(shouldGravityHappen(player.x, player.y, earth2)){
		gravitateToObject(player, earth2);
	}
	// check for if there is a stable orbit on the closest celestial body
	if(getDistance(player, earth) > getDistance(player, earth2)){
		if(isStableOrbit(player, earth2)){
			setToOrbit(player, earth2);
		}
	}
	else{
		if(isStableOrbit(player,earth)){
			setToOrbit(player, earth);
		}
	}
	
	
	//MINIMAP LOGIC
	miniship.x = player.x;
	miniship.y = player.y;
	miniship.anchor.set(.5);
	miniship.angle = ship.spr.angle;
	minimap.update();
	
	if(game.time.totalElapsedSeconds() > 1 && !earth.mapped && earth.inCamera){
		minimap.addChild(game.make.sprite(earth.x, earth.y, 'earth'));
		earth.mapped = true;
	}
	
	if(game.time.totalElapsedSeconds() > 1 && !earth2.mapped && earth2.inCamera){
		minimap.addChild(game.make.sprite(earth2.x, earth2.y, 'planet1'));
		earth2.mapped = true;
	}
};

function setToOrbit(player, bigobject){
	player.x = bigobject.x + 200;
	console.log(player.x);
	player.y = bigobject.y + 200;
	console.log(player.y);
}
function gravitateToObject(obj1, obj2, gravPow) {
    if (typeof speed === 'undefined') { gravPow = 10000000; }
    var angle = Math.atan2(obj2.y - obj1.y, obj2.x - obj1.x);
    rdistance = Phaser.Point.distance(obj1, obj2);
    gravF = (gravPow / (rdistance * rdistance));
    obj1.body.force.x += Math.cos(angle) * gravF;    // accelerateToObject 
    obj1.body.force.y += Math.sin(angle) * gravF;
}
function getDistance(player, bigobject){
	var xdist = 0;
	var ydist = 0;
	xdist = Math.pow(player.x - bigobject.x,2);
	ydist = Math.pow(player.x - bigobject.y,2);
	var totaldist = xdist + ydist;
	totaldist = Math.sqrt(totaldist);
	return totaldist;
}
/*	returns true if the celestial object should activate its gravity on the spaceship
*   @param	playerposx, x position of the player
*	@param playerposy, y position of the player
*	@param bigobject, the celestial object
*/
function shouldGravityHappen(playerposx, playerposy, bigobject){
	if(playerposx < bigobject.x - 400 || playerposx > bigobject.x + 400){
		return false;
	}
	if(playerposy < bigobject.y - 400 || playerposy > bigobject.y + 400){
		return false;
	}
	return true;
}
// Returns if a collision is true
function isCollision(player,bigobject){
	if(player.x > bigobject.x - 90 && player.x < bigobject.x + 90){
		if(player.y > bigobject.y - 90 && player.y < bigobject.y + 90){
			return true;
		}
	}
	return false;
}
//used for the stable orbit detection, must stay within this range from the planet
function isInRange(player,bigobject){
	if(player.x > bigobject.x - 300 && player.x < bigobject.x + 300){
		if(player.y > bigobject.y - 300 && player.y < bigobject.y + 300){
			return true;
		}
	}
	return false;
}
function isStableOrbit(player, bigobject){
	if(!isInRange(player,bigobject)){
		stableorbitcounter = 0;
		singleorbitcounter = 0;
		currentsector = null;
		orbitdirection = null;
		//console.log("not in range");
		return false;
	}
	// find which sector its in
	//if its in the same sector do nothing
	// check it goes to a clockwise or anti clockwise sector
	
	// top left sector
	if(player.x < bigobject.x && player.y < bigobject.y){
		if(currentsector === "top left"){
			return false;
		}
		if(singleorbitcounter > 0){
			if(currentsector === "bottom left"){
				singleorbitcounter++;
				currentsector = "top left";
				console.log("bot left to top left");
				//console.log("orbitcounter: " + singleorbitcounter);
				if(orbitdirection === "counterclockwise"){
					singleorbitcounter = 0;
					stableorbitcounter = 0;
					console.log("direction reset");
				}
				orbitdirection = "clockwise";
			}
			else if(currentsector === "top right"){
				singleorbitcounter++;
				currentsector = "top left";
				console.log("top right to top left");
				//console.log("orbitcounter: " + singleorbitcounter);
				if(orbitdirection === "clockwise"){
					singleorbitcounter = 0;
					stableorbitcounter = 0;
					console.log("direction reset");
				}
				orbitdirection = "counterclockwise";
			}
			else{
				singleorbitcounter = 0;
				stableorbitcounter = 0;
				currentsector = "top left";
			}
		}
		if(singleorbitcounter === 0){
			currentsector = "top left";
			singleorbitcounter++;
			//console.log("single orbit top left");
		}
		if(singleorbitcounter >= 4){
			stableorbitcounter++;
			singleorbitcounter = 0;
			//console.log("single orbit counter is at 4");
		}
		if(stableorbitcounter === 4){
				stableorbitcounter = 0;
				return true;
		}
		//console.log("top left");
	}
	// bottom left sector
	if(player.x < bigobject.x && player.y > bigobject.y){
		if(currentsector === "bottom left"){
			return false;
		}
		if(singleorbitcounter > 0){
			if(currentsector === "bottom right"){
				singleorbitcounter++;
				currentsector = "bottom left";
				console.log("bot right to bot left");
				//console.log("orbitcounter: " + singleorbitcounter);
				if(orbitdirection === "counterclockwise"){
					singleorbitcounter = 0;
					stableorbitcounter = 0;
					console.log("direction reset");
				}
				orbitdirection = "clockwise";
			}
			else if(currentsector === "top left"){
				singleorbitcounter++;
				currentsector = "bottom left";
				console.log("top reft to bottom left");
				//console.log("orbitcounter: " + singleorbitcounter);
				if(orbitdirection === "clockwise"){
					singleorbitcounter = 0;
					stableorbitcounter = 0;
					console.log("direction reset");
				}
				orbitdirection = "counterclockwise";
			}
			else{
				singleorbitcounter = 0;
				stableorbitcounter = 0;
				currentsector = "bottom left";
			}
		}
		if(singleorbitcounter === 0){
			currentsector = "bottom left";
			singleorbitcounter++;
			//console.log("single orbit bottom left");
		}
		if(singleorbitcounter >= 4){
			stableorbitcounter++;
			singleorbitcounter = 0;
			//console.log("single orbit counter is at 4");
		}
		if(stableorbitcounter === 4){
				stableorbitcounter = 0;
				return true;
		}
		//console.log("bottom left");
	}
	
	// top right sector
	if(player.x > bigobject.x && player.y < bigobject.y){
		if(currentsector === "top right"){
			return false;
		}
		if(singleorbitcounter > 0){
			if(currentsector === "top left"){
				singleorbitcounter++;
				currentsector = "top right";
				console.log("top left to top right");
				//console.log("orbitcounter: " + singleorbitcounter);
				if(orbitdirection === "counterclockwise"){
					singleorbitcounter = 0;
					stableorbitcounter = 0;
					console.log("direction reset");
				}
				orbitdirection = "clockwise";
			}
			else if(currentsector === "bottom right"){
				singleorbitcounter++;
				currentsector = "top right";
				console.log("bottom right to top right");
				//console.log("orbitcounter: " + singleorbitcounter);
				if(orbitdirection === "clockwise"){
					singleorbitcounter = 0;
					stableorbitcounter = 0;
					console.log("direction reset");
				}
				orbitdirection = "counterclockwise";
			}
			else{
				singleorbitcounter = 0;
				stableorbitcounter = 0;
				currentsector = "top right";
			}
		}
		if(singleorbitcounter === 0){
			currentsector = "top right";
			singleorbitcounter++;
			//console.log("single orbit top right");
		}
		if(singleorbitcounter >= 4){
			stableorbitcounter++;
			singleorbitcounter = 0;
			//console.log("single orbit counter is at 4");
		}
		if(stableorbitcounter === 4){
				stableorbitcounter = 0;
				return true;
		}
		//console.log("top right");
	}
	// bottom right sector
	if(player.x > bigobject.x && player.y > bigobject.y){
		if(currentsector === "bottom right"){
			return false;
		}
		if(singleorbitcounter > 0){
			if(currentsector === "top right"){
				singleorbitcounter++;
				currentsector = "bottom right";
				console.log("top right to bottom right");
				//console.log("orbitcounter: " + singleorbitcounter);
				if(orbitdirection === "counterclockwise"){
					singleorbitcounter = 0;
					stableorbitcounter = 0;
					console.log("direction reset");
				}
				orbitdirection = "clockwise";
			}
			else if(currentsector === "bottom left"){
				singleorbitcounter++;
				currentsector = "bottom right";
				console.log("bottom left to bottom right");
				//console.log("orbitcounter: " + singleorbitcounter);
				if(orbitdirection === "clockwise"){
					singleorbitcounter = 0;
					stableorbitcounter = 0;
					console.log("direction reset");
				}
				orbitdirection = "counterclockwise";
			}
			else{
				singleorbitcounter = 0;
				stableorbitcounter = 0;
				currentsector = "bottom right";
			}
		}
		if(singleorbitcounter === 0){
			currentsector = "bottom right";
			singleorbitcounter++;
			//console.log("single orbit bottom right");
		}
		if(singleorbitcounter >= 4){
			stableorbitcounter++;
			singleorbitcounter = 0;
			//console.log("single orbit counter is at 4");
		}
		if(stableorbitcounter === 4){
				stableorbitcounter = 0;
				return true;
		}
		//console.log("bottom right");
		
	}
	
	
}
function render() {
	this.game.debug.text("distance from earth: " + rdistance, 75, 150);
	this.game.debug.text("force of gravity: " + gravF, 75, 175);
    this.game.debug.cameraInfo(game.camera, 32, 32);
    this.game.debug.spriteCoords(player, 32, 500);
	this.game.debug.text("total circles in orbit: " + stableorbitcounter, 75, 200);
	this.game.debug.text("orbit direction: " + orbitdirection, 75, 225);
	// FIX THIS BEFORE YOU RUN THE CODE, need to iomplement the circle collision and not square
	
	if(isCollision(player,earth)){
		//console.log("earth collision");
	}
	if(isCollision(player,earth2)){
		//console.log("earth2 collision");
	}
}


/*----------Main.js----------
 * Contains game logic code.
 * --------------------------
 * Author: Ashwin Balaji
 * Created: 2/21/2015
 * CMPM 20 Winter 2015
 * --------------------------
 */

/*
window.onload = function(){
	var game = new Phaser.Game(1600,900,Phaser.AUTO, '',{preload: preload, create: create, update: update},false,true);
	
	//global health variable
	
	var ship = new Ship(game);
	var gui = new GameUI(game);
	var cursors;
	
	function preload() {
		//sprites with animations need to load
		//animation data in JSONArray or JSONHash.
		
		//This implementation uses JSONArray -- (MOVED TO GAMEUI.JS)
		
		game.load.image('backdrop', 'space_stars_backdrop.png');
		ship.preloadShip();
		gui.preloadGUI();
	}
	
	function create(){
		game.add.tileSprite(0,0,4096,4096,'backdrop');
		game.world.setBounds(0,0,4096,4096);
		
		//NOTE: LOGIC MOVED TO GAMEUI.JS
		//Create a test sprite for animation
		//var testAnim = game.add.sprite(200,200,'test');
		//call function
		//setHealth(50,testAnim);
	
		console.log(ship);
		ship.initShip();
		gui.initGUI();
		
		
		game.physics.startSystem(Phaser.Physics.P2JS);
		
		cursors = game.input.keyboard.createCursorKeys();
    	player = ship.spr;//game.add.sprite(32, game.world.height - 150, 'ship');
    	//earth = game.add.sprite(500, 500, 'earth');
    	//game.physics.p2.enable([player,earth], true);
    	game.physics.p2.enable(player);
    	player.body.immovable = true;
    	//ship.addFlames();
    	//earth.body.setCircle(80);
    	//earth.body.static = true;
    	game.camera.follow(player);	
    	//testAnim.fixedToCamera = true;
    	
    	setTimeout(function(){
    		gui.health.setBarVal(50,gui.health.spr,ship.health);
    	},4000);
		
	}
	
	var limit = true;
	
	function update(){
		if (cursors.left.isDown) {player.body.rotateLeft(100);}   //ship movement
    	else if (cursors.right.isDown){player.body.rotateRight(100);}
    	else {player.body.setZeroRotation();}
    	
    	if (cursors.up.isDown){player.body.thrust(400);}
    	else if (cursors.down.isDown){player.body.reverse(400);}
    	
    	if((cursors.up.isDown || cursors.down.isDown)){
    		if(limit == true ){
    			if(!cursors.down.isDown)ship.startFlames();
    		}
    		limit = false;
    	}
    	else {ship.stopFlames(); limit = true;}
    	
    //gravitateToObject(player, earth);
	}
};*/