/*----------Ship.js----------
 * Contains code for the ship.
 * --------------------------
 * Author: Ashwin Balaji
 * Created: 2/21/2015
 * CMPM 20 Winter 2015
 * --------------------------
 */

/*--Ship()--
 * Constructs a ship object.
 * Param: 
 * 		g: a reference to the current game object
 * Return: (Constructor) a new Ship object.
 */
function Ship(g){
	
	//*****Class Variables******//
	
	//reference to game object	
	this.game = g;
	//Sprite of ship object
	this.spr = null;
		
	//*****Ship Sounds*****//
	//WIP
	//NOTE: Will be an object constructed using
	//			initSound() function.
	this.sounds = null;
	
	//*****States*****//
	
	//Ship states to determine sounds/images
	//NOTE: May be implicit or handled in Main.js
	//		and therefore unecessary.Retain until 
	//		alternative is in place.
	//this.states = {idle: 0, moving: 1, orbiting: 2, boosting: 3};
	
	//Anchor points for attachments
	this.anchors = {};
	
	//Handles to attachments
	this.attachments = {};
	
	//Numeric health value
	this.health = 100;
	
	//Numeric Fuel Value
	this.fuel = 100;
	
	this.animScale = 1;
	//*****Functions************//
	
	/*--preloadShip()--
	 * Contains the code used in the game's preload state.
	 * Param: none
	 * Return: none
	 * WIP: NOTE: This preload still needs sound and attachments other than fire!
	 */
	Ship.prototype.preloadShip = function(){
		//Add all assets here
		this.game.load.image('pRocket','rocket_spr.png');
		
		//flames test
		this.game.load.atlasJSONArray('flames','flames_spr.png','flames.json');
		
		//Misc
		
		//Used for setFlameAnimation()
		this.arNum = [];
		this.arNum2 = [];
		for(var i = 0; i<=120; i++)this.arNum.push(i);
		for(var i = 120; i<=240; i++)this.arNum2.push(i);
	};
	
	/*--initAnchors()--
	 * Sets the anchor points for effect attachment.
	 * Param: none
	 * Return: none
	 */
	Ship.prototype.initAnchors = function(){
		this.anchors['front'] = new Phaser.Point(0,-500);
		this.anchors['back']  = new Phaser.Point(0,700);
		this.anchors['l_back'] = new Phaser.Point(-150,450);
		this.anchors['r_back'] = new Phaser.Point(150,450);
		
		/* Old values for .5 scale 
		 this.anchors['front'] = new Phaser.Point(0,-500);
		this.anchors['back']  = new Phaser.Point(0,500);
		this.anchors['l_back'] = new Phaser.Point(-70,300);
		this.anchors['r_back'] = new Phaser.Point(70,300);
		 */
	};
	
	/*--setFlameAnimation()--
	 * Creates the animation steps for the flames.
	 * NOTE: Only to be used with sprites containing
	 * 			the flame animation!
	 * Param: 
	 * 		spr: Sprite: the reference to the flame sprite
	 * Return: none
	 */
	Ship.prototype.setFlameAnimation = function(spr){
		spr.animations.add('startboost',this.arNum,30*this.animScale,false,true);
		spr.animations.add('boost',this.arNum2,60*this.animScale,true,true);
	};
	
	/*--addFlames()--
	 * Adds the flame attachements as children of
	 * the ship and then displays to the world.
	 * Param: none
	 * Return: none
	 */
	Ship.prototype.addFlames = function(){
		//Center
		
		this.attachments['flame_center']=(this.game.add.sprite(this.anchors['back'].x,this.anchors['back'].y,'flames'));
		
		this.spr.addChild(this.attachments['flame_center']);		
		
		this.attachments['flame_center'].anchor.set(.5);
		this.attachments['flame_center'].width = 512;
		this.attachments['flame_center'].height = 1024;
		this.attachments['flame_center'].angle = 180;
		
		this.setFlameAnimation(this.attachments['flame_center']);
		
		//Left
		this.attachments['flame_l'] = this.game.add.sprite(this.anchors['l_back'].x, this.anchors['l_back'].y, 'flames');
		
		this.spr.addChild(this.attachments['flame_l']);
		
		this.attachments['flame_l'].anchor.set(.5);
		this.attachments['flame_l'].width = 512;
		this.attachments['flame_l'].height = 512;
		this.attachments['flame_l'].angle = 190;
		
		this.setFlameAnimation(this.attachments['flame_l']);
		
		//Right
		this.attachments['flame_r'] = this.game.add.sprite(this.anchors['r_back'].x, this.anchors['r_back'].y, 'flames');
		
		this.spr.addChild(this.attachments['flame_r']);
		
		this.attachments['flame_r'].anchor.set(.5);
		this.attachments['flame_r'].width = 512;
		this.attachments['flame_r'].height = 512;
		this.attachments['flame_r'].angle = 170;
		
		this.setFlameAnimation(this.attachments['flame_r']);
	
	};
	
	/*--stopFlames()--
	 * Stops the flame attachment animations.
	 * Param: none
	 * Return: none
	 */
	Ship.prototype.stopFlames = function(){
		this.attachments['flame_center'].animations.play('startboost');
		this.attachments['flame_center'].animations.stop();
		
		this.attachments['flame_l'].animations.play('startboost');
		this.attachments['flame_l'].animations.stop();
		
		this.attachments['flame_r'].animations.play('startboost');
		this.attachments['flame_r'].animations.stop();
	};
	
	/*--startFlames()--
	 * Starts the flame attachment animations.
	 * Param: none
	 * Return: none
	 */
	Ship.prototype.startFlames = function(){
		var spr = this.attachments;
		spr['flame_center'].animations.play('startboost');
		spr['flame_l'].animations.play('startboost');
		spr['flame_r'].animations.play('startboost');
		
		setTimeout(function(){
			spr['flame_center'].animations.play('boost');
			spr['flame_l'].animations.play('boost');
			spr['flame_r'].animations.play('boost');
		},1000);
	};
	
	/*--initShip()--
	 * Initializes the ship and the flame attachments.
	 * Param: none
	 * Return: none
	 * WIP
	 */
	Ship.prototype.initShip = function(){
		//Ship Sprite
		//this.spr = this.game.add.sprite(this.game.camera.width/2,this.game.camera.height/2,'pRocket');
		this.spr = this.game.add.sprite(this.game.world.width,0,'pRocket');
		this.spr.anchor.set(.5,.75);
		this.spr.width = 32;
		this.spr.height= 64;
		
		//initialize anchor points
		this.initAnchors();
		//this.addFlames();
		
	};
	
	
	
	/*--initSound()--
	 * Initializes all ship sounds.
	 * Param:
	 * 		sName : Array: Names of sound event (ex: start,moving,damaged,etc.)
	 * 		sFiles: Array: Audio files associated with each corresponding entry in 
	 * 						sName.
	 * Return: Object
	 */
	this.initSound = function(sName,sFiles){
		if(sName.length != sFiles.length)throw "Error @ initSound(): parameter arrays have different lengths!";
		else{
			var sounds = {};
			for(var i = 0; i<sName.length;i++)sounds[sName[i]] = sFiles[i];
			return sounds;
		}
	};
	
}


/*
//Preload graphics
		//WIP
		
		//Assume assets are loaded and add sprite to game.
		this.spr = this.game.add.group(); 
		this.spr.create(100,100,'pRocket');		
		
		//Default width/height
		this.spr.width = 32;
		this.spr.height = 64;
		
		//set the sprite's center
		/*this.spr.pivot.x = this.spr.x - this.spr.width/2;
		this.spr.pivot.y = this.spr.y - this.spr.height/2;*/
		/*this.spr.pivot.setTo(.5);
		
		//set attachment anchors
		this.anchors['front'] 	= new Phaser.Point(this.spr.x,this.spr.y - this.spr.height/2);
		this.anchors['back']	= new Phaser.Point(this.spr.x ,this.spr.y +12*0+ this.spr.height/2);
		this.anchors['left']	= new Phaser.Point(this.spr.x - 10, this.spr.pivot.y);
		this.anchors['right']	= new Phaser.Point(this.spr.x + 10, this.spr.pivot.y);
		
		//this.attachments['thruster_l1'] = this.game.add.sprite(this.anchors['back'].x, this.anchors['back'].y, 'flames');
		
		this.attachments['thruster_l1'] = this.game.add.sprite(this.anchors['back'].x,this.anchors['back'].y,'flames');
		
		this.attachments['thruster_l1'].angle = 180;
		
		this.attachments['thruster_l1'].width = 32;
		this.attachments['thruster_l1'].height = 16;
		this.attachments['thruster_l1'].anchor.setTo(.5);
		
		var arNum = [];
		var arNum2 = [];
		for(var i = 0; i<=120; i++)arNum.push(i);
		for(var i = 120; i<=240; i++)arNum2.push(i);
		
		this.attachments['thruster_l1'].animations.add('startboost',arNum,60,false,true);
		this.attachments['thruster_l1'].animations.add('boost',arNum2,60,true,true);
		
		this.attachments['thruster_l1'].animations.play('startboost');
		this.attachments['thruster_l1'].animations.play('boost');
		
		this.spr.angle = 90;
		//Input handles
		this.spr.inputEnabled = true;
		*/
