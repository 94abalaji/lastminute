var game = new Phaser.Game(1600, 900, Phaser.CANVAS, 'phaser-example', { preload: preload, create: create, update:update, render: render });
var ship = new Ship(game);
var gui = new GameUI(game);
var cursors;

var planetCollisionGroup;
var playerCollisionGroup;
var asteroidCollisionGroup;
var minimap;
var miniship;

var p1;
var propM;

var lock = true;
var fuelSub = 0;
var thrust;
var scansound;
var errsound;

function preload(){
	game.load.image('backdrop', 'space_stars_backdrop.png');
	game.load.image('mm_backdrop', 'stars_mm.png');
	game.load.image('miniship', 'rocket_spr_down.png');
   game.load.bitmapFont('upheaval', 'upheaval.png', 'upheaval.fnt');
   //music
   game.load.audio('serene','serene.mp3');
   var serene = game.add.audio('serene');
   //thrust sound
   game.load.audio('thrust','thrust.mp3');
   thrust = game.add.audio('thrust');
   //scanning sound
   game.load.audio('scansound','scan.mp3');
   scansound = game.add.audio('scansound');
   //error sound
   game.load.audio('errsound','error2.mp3');
   errsound = game.add.audio('errsound');
   
   
	preloadPlanets();
	preloadProps();
	ship.preloadShip();
	gui.preloadGUI();
	serene.play('',0,1,true,true);
}

function create(){
	//Background
	game.add.tileSprite(0,0,8192,8192,'backdrop');
	
	//World bounds
	//game.world.setBounds(0,0,4096,4096);
	game.world.setBounds(0,0,8192,8192);
	
	//Initialize ship and ui
	ship.initShip();
	gui.initGUI();
	
	//Initialize physics module
	game.physics.startSystem(Phaser.Physics.P2JS);
	game.physics.p2.setImpactEvents(true);
   playerCollisionGroup = game.physics.p2.createCollisionGroup();
   planetCollisionGroup = game.physics.p2.createCollisionGroup();
   asteroidCollisionGroup = game.physics.p2.createCollisionGroup();
   game.physics.p2.updateBoundsCollisionGroup();
	//Initialize input keys
	cursors = game.input.keyboard.createCursorKeys();
	
	//Create planets	
	p1 = new PlanetManager(game);
	p1.populateList(10);
	p1.resolveOverlap();
	p1.setEarthLoc();
	
	//Create Props
	propM = new PropManager(game);
	propM.populateList(100);
	
	//Apply physics to ship.spr
	game.physics.p2.enable(ship.spr, false);
	//ship.spr.body.immovable = true;
	game.camera.follow(ship.spr);
	ship.spr.body.setCollisionGroup(playerCollisionGroup);
   ship.spr.body.collides(planetCollisionGroup, hitPlanet, this);
   ship.spr.body.collides(asteroidCollisionGroup, hitAsteroid, this);
	//Initialize minimap UI component
	minimap = new UIObject(0);
	minimap.parent = gui;
	minimap.spr = game.add.tileSprite(0,0,8192,8192,'mm_backdrop');
	minimap.spr.tileScale.setTo(3);
	minimap.spr.fixedToCamera = true;
	minimap.spr.scale.setTo(.03);
	minimap.spr.cameraOffset.setTo(game.camera.width - 250, 660);
	
	//Attach miniship to minimap
	miniship = minimap.spr.addChild(game.make.sprite(ship.spr.x,ship.spr.y,'miniship'));
	
	//Add flames to ship
	ship.addFlames();
   
	pause_txt = game.add.bitmapText(65, game.height - 75, 'upheaval', 'PAUSE - P', 32);
   mvmnt_txt = game.add.bitmapText(65, game.height - 40, 'upheaval', 'MOVEMENT - ARROW KEYS', 32);
   pauseLabel = game.add.bitmapText(180, 330, 'upheaval', '        * GAME PAUSED *\n\nclick anywhere to continue', 75);
   // scan text 
   scan_txt = game.add.bitmapText(600, game.height-100, 'upheaval', '    SCANNING   ', 60);
   scan_txt.fixedToCamera = true;
   // added fuel, planet not found text
   fuel_txt = game.add.bitmapText(70, game.height-200, 'upheaval', '   ABILITY TO SUPPORT LIFE: NEGATIVE, FUEL FOUND   ', 32);
   fuel_txt.fixedToCamera = true;
   // new earth has been found
   earth_txt = game.add.bitmapText(70, game.height-200, 'upheaval', '   LIVEABLE PLANET FOUND, CONTACTING EARTH  ', 35);
   earth_txt.fixedToCamera = true;
   over_txt = game.add.bitmapText(70, game.height-300, 'upheaval', '  GAME OVER ', 50);
   over_txt.fixedToCamera = true;
   over_txt.visible = false;
   fuel_txt.visible = false;
   earth_txt.visible = false;
   // pause logic
   pauseLabel.fixedToCamera = true;
   pauseLabel.visible = false;
   pause_txt.fixedToCamera = true;
   mvmnt_txt.fixedToCamera = true;
   
   pause_key = game.input.keyboard.addKey(Phaser.Keyboard.P);
   pause_key.onDown.add(pauseGame, this);
   game.input.onDown.add(pauseGame, this);
}
function pauseGame(event) {
    if (game.paused == false){
       game.paused = true;
       pauseLabel.visible = true;
    }else {
       pauseLabel.visible = false;
       game.paused = false;
    }
}
function update(){
	//INPUT KEY LOGIC
	if (cursors.left.isDown) {ship.spr.body.rotateLeft(100);}   //ship movement
    else if (cursors.right.isDown){ship.spr.body.rotateRight(100);}
    else {ship.spr.body.setZeroRotation();}
    if (cursors.up.isDown && ship.fuel>0)
		{ship.spr.body.thrust(400);
		 thrust.play('',0,1,false,false);
		}
    else if (cursors.down.isDown && ship.fuel>0){ship.spr.body.reverse(400);}
	else{
		thrust.stop();
	}
    
    //THRUSTER ANIMATION LOGIC
    if((cursors.up.isDown || cursors.down.isDown) && ship.fuel>0){
        if(lock){
            if(!cursors.down.isDown)ship.startFlames();
        }
        lock = false;
        
        fuelSub += 1;
        if(fuelSub>100){
        	gui.health.setBarVal(ship.fuel,gui.fuel.spr,--ship.fuel);
			ship.fuel -= 3;
        	fuelSub = 0;
        	//console.log('ship fuel: ' + ship.fuel);
        }
        
    }
    else {ship.stopFlames(); lock = true;}
    
    var closestPlanet = p1.getClosestPlanetTo(ship.spr);
    
    //console.log('nearest planet dist: ' + getDistance(ship.spr,closestPlanet.spr));
    
    //GRAVITY LOGIC
    if(shouldGravityHappen(ship.spr.x,ship.spr.y,closestPlanet.spr)){
    	gravitateToObject(ship.spr,closestPlanet.spr);
    }
       
       
    //if(getDistance(ship.spr,p1.spr) <1.5*p1.spr.width){
    if(isInRange(ship.spr,closestPlanet.spr)){
    	if(isStableOrbit(ship.spr, closestPlanet.spr))setToOrbit(ship.spr, closestPlanet);
      var delta_y = ship.spr.y - closestPlanet.spr.y;
      var delta_x = ship.spr.x - closestPlanet.spr.x;
      orbitangle = Math.atan(delta_y/delta_x) * (180/Math.PI);
    }
    /*
	*	Game Logic 3-11
	* To do, implement more SetToOrbit, settoorbit should either give fuel or win the game.
	*	add after 2 orbits here a message, and a sound.
	*/
   if(stableorbitcounter >= 2){
	   scan_txt.visible = true;
	   scansound.play('',0,1,true,false);
   }
   else{
	   scan_txt.visible = false;
	   scansound.stop();
   }
   if(ship.health <= 0){
	   over_txt.visible = true;
   }
   //p1.getClosestPlanetTo(ship.spr);
   
    //MINIMAP LOGIC
    
    miniship.x = ship.spr.x + 180;
    miniship.y = ship.spr.y + 180;
    miniship.anchor.setTo(.5);
    miniship.angle = ship.spr.angle;
    
    //Test minimap alert
    //p1.alertMinimap(minimap);
    p1.alertUI(minimap);
}

function render() {
	/*this.game.debug.text("distance from earth: " + rdistance, 75, 150);
	this.game.debug.text("force of gravity: " + gravF, 75, 175);
    this.game.debug.cameraInfo(game.camera, 32, 32);
    this.game.debug.spriteCoords(ship.spr, 32, 500);
	this.game.debug.text("total circles in orbit: " + stableorbitcounter, 75, 200);
	this.game.debug.text("orbit direction: " + orbitdirection, 75, 225);
	if(isInRange(ship.spr,p1.earthLoc))console.log("earth in range");*/
	// FIX THIS BEFORE YOU RUN THE CODE, need to iomplement the circle collision and not square
	
	/*if(isCollision(ship.spr,earth)){
		//console.log("earth collision");
	}
	if(isCollision(ship.spr,earth2)){
		//console.log("earth2 collision");
	}*/
}

