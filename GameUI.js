/*----------GameUI.js----------------
 * Contains code for the UI.
 * ----------------------------------
 * Author: Ashwin Balaji, Justin Liu
 * Created: 2/25/2015
 * CMPM 20 Winter 2015
 * ----------------------------------
 */

function GameUI(game) {

	//The list of UI Objects
	this.members = [];
	this.game = game;
	//The mandatory UI Objects first:
	this.health = null;
	this.fuel = null;
	this.oxygen = null;
	this.time = null;

	GameUI.prototype.preloadGUI = function() {
		//All art assets required for UI are
		//	added here.

		this.game.load.atlasJSONArray('bar1', 'bar.png', 'bar.json');
		this.game.load.atlasJSONArray('bar2', 'bar2.png', 'bar2.json');
	};

	GameUI.prototype.initGUI = function() {
		//Create mandatory UI Objects
		this.health = new UIObject(1);
		this.fuel = new UIObject(1);
		this.oxygen = new UIObject(1);
		this.time = new UIObject(2);
		
		//Initialize values
		
		//health
		this.health.spr = this.game.add.sprite(this.game.camera.width - 200, -100, 'bar1');
		//console.log(this.health.spr.position);
		this.health.hasAnimations = true;
		this.health.linkedToCamera = true;
		this.health.parent = this;
		
		this.fuel.spr = this.game.add.sprite(this.game.camera.width - 200, -50, 'bar2');
		//console.log(this.fuel.spr.position);
		this.fuel.hasAnimations = true;
		this.fuel.linkedToCamera = true;
		this.fuel.parent = this;
		
		this.health.update();
		this.fuel.update();
	};

	//UI Object Nested class

}

	function UIObject(type) {
		//Different types of UIObjects
		UIObject.prototype.objType = {
			image : 0,
			bar : 1,
			time : 2,
			button : 4
		};
		
		//Type of UI Object instance
		this.type = null;
		
		//Set type based on input parameter
		switch(type){
			case 0: this.type = this.objType.image;break;
			case 1: this.type = this.objType.bar;break;
			case 2: this.type = this.objType.time;break;
			case 3: this.type = this.objType.button;break;
		}
		
		//Sprite to display (if applicable)
		this.spr = null;

		//True if the object has animations.
		this.hasAnimations = false;

		//True if attached and follows camera
		this.linkedToCamera = false;

		//Parent container reference
		this.parent = null;

		/*--setBarVal()--
		 * The function which simulates the bar animation.
		 * Can be used with any bar.
		 * Param:
		 * 		h :	scalar: value to set to.
		 * 		sp:	Object: the bar sprite whose value
		 * 						is to be changed.
		 * 		health: scalar: value to compare to.
		 * Returns: none
		 */

		/*
		 * NOTE: Dependency: this implementation assumes a
		 * 			global 'health' variable. This should be
		 * 			set to the ship health in the actual game
		 * 			implementation.
		 */
		UIObject.prototype.setBarVal = function(h, sp, health) {
			var t1 = null;
			if (h < health) {
				t1 = setInterval(function() {
					if (h >= health)
						clearInterval(this);
					else {
						health--;
						sp.frame = 100 - health;
					}
				}, 30);
			} else if (h > health) {
				t1 = setInterval(function() {
					if (h <= health)
						clearInterval(this);
					else {
						health++;
						sp.frame = 100 - health;
					}
				}, 30);

			}
		};
		
		UIObject.prototype.update = function(){
			if(this.linkedToCamera)this.spr.fixedToCamera = true;
			
			if(this.type = this.objType.time){
				this.timer = {
					tFn: null,
					tInterval: null,
					gTimer: null,
					total: 0
				};
				this.timer.start = function(){
					this.timer.text = this.parent.game.make.text(0,0,total,{ font: "bold 32px Arial", fill: "#ff0044" });
					this.timer.gTimer = setInterval(this.timer.tFn,this.timer.tInterval);
				};
				this.timer.stop = function(){
					clearInterval(this.timer.gTimer);
				};
				
			}
		};
	

	}

